// SPDX-FileCopyrightText: 2021-2023 Arthur Brainville (Ybalrid) <ybalrid@ybalrid.info>
//
// SPDX-License-Identifier: MIT
//
// Initial Author: Arthur Brainville <ybalrid@ybalrid.info>

#pragma once
#ifdef _MSC_VER
#pragma warning(disable : 26812) //OpenXR is a C API that doesn't use `enum class`
#endif

#include "openxr/openxr.h"
