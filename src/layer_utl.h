// SPDX-FileCopyrightText: 2024 mittorn <mittorn@disroot.org>
//
// SPDX-License-Identifier: MIT
//
#pragma once
#include "container_utl.h"
#define FMT_ENABLE_STDIO
#include "fmt_util.h"
#include "string_utl.h"
#include "struct_utl.h"

// fmt_util extension
template<typename Buf>
forceinline constexpr static inline void ConvertS(Buf &s, const SubStr &arg)
{
	for(const char *str = arg.begin; str < arg.end;str++)
		s.w(*str);
}

// hashmap extension

template<>
inline bool KeyCompare(const SubStr &a, const SubStr &b)
{
	return a.Equals(b);
}

//inline const SubStr &KeyAlloc<SubStr>(const SubStr &a) = delete;

inline const SubStr KeyAlloc(const SubStr &a)
{
	return a.StrDup();
}

template<>
inline void KeyDealloc(const SubStr &a)
{
	free((void*)a.begin);
}

template<size_t TblSize>
forceinline static inline size_t HashFunc(const SubStr &key)
{
	size_t hash = 7;
	const unsigned char *s = (const unsigned char*)key.begin;
	while(s < (const unsigned char*)key.end)
		hash = hash * 31 + *s++;
	return hash & (TblSize - 1);
}

template <size_t l>
constexpr size_t ConstHashFunc(const char (&ch)[l])
{
	size_t hash = 7;
	for(int i = 0; i < l - 1; i++)
		hash = hash * 31 + ch[i];
	return hash;
}

template <typename DumperType>
struct Dumper
{
	char *base;
	char *tmpbase;
	int index = -1;
	void SetIndex(int idx)
	{
		index = idx;
	}
	template <typename T>
	T& GetData(T* tempptr)
	{
		if(index == 0)
		{
			tmpbase = (char*)tempptr;
			d.Begin();
		}
		return *(T*)(base + ((char*)tempptr - tmpbase));
	}
	void End(size_t sz)
	{
		d.End();
	}
	DumperType &d;
	Dumper(DumperType &d1) : d(d1){}
};

template <typename DumperType, typename T>
void DumpNamedStruct(DumperType &d, const T *data)
{
	Dumper<DumperType> t = {d};
	t.base = (char*)data;
	ConstructorVisitor<T, Dumper<DumperType>>().Construct(t);
}

static constexpr const char *const gszUserSuffixes[] =
	{
		"left",
		"right",
		"head",
		"gamepad",
		//"/user/treadmill"
};


enum eUserPaths{
	USER_HAND_LEFT = 0,
	USER_HAND_RIGHT,
	USER_HEAD,
	USER_GAMEPAD,
	USER_INVALID,
	USER_PATH_COUNT
};

#define Log(...) FPrint(stderr, __VA_ARGS__)
