#include <arpa/inet.h>
#include <sys/socket.h>
#include "imgui_impl_opengl2.h"
#include <GL/gl.h>
#include "layer_events.h"

#if USE_SDL == 2
#include "SDL.h"
#include "imgui_impl_sdl2.h"
#include "imgui_impl_sdlrenderer2.h"
static struct
{
	bool useGL;
	SDL_Renderer *renderer;
	SDL_Window *window;
	SDL_GLContext context;
	void (APIENTRY*pglClear)(GLbitfield mask);
	void (APIENTRY*pglClearColor)(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha);
	void (APIENTRY*pglViewport)(GLint x, GLint y, GLsizei width, GLsizei height);
	void (APIENTRY*pglDisable)(GLenum cap);
	void (APIENTRY*pglEnable)(GLenum cap);
} gPlatformState;
void glDisable(GLenum cap);

void *DYN_GL_GetProcAddress(const char *proc)
{
	return SDL_GL_GetProcAddress(proc);
}

bool Platform_Init( const char *title, int width, int height, bool preferSoftware )
{
	// Setup SDL
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		printf("Error: %s\n", SDL_GetError());
		return false;
	}
	SDL_Init(SDL_INIT_TIMER);

#ifdef SDL_HINT_IME_SHOW_UI
	SDL_SetHint(SDL_HINT_IME_SHOW_UI, "1");
#endif

	SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
	gPlatformState.window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, window_flags);
	if (gPlatformState.window == nullptr)
	{
		Log("Error: SDL_CreateWindow(): %s\n", SDL_GetError());
		return false;
	}
	static bool bErrored = false;
	bool bIsX11 = !strcmp(SDL_GetCurrentVideoDriver(), "x11");
	// SDL does not handle X11 errors, so app crashes if GL renderer unavailiable
	if(!strcmp(SDL_GetCurrentVideoDriver(), "x11"))
	{
		// anyway, 2d rendering in software and uploading to GL is slower than direct x11 render
		SDL_SetHint("SDL_FRAMEBUFFER_ACCELERATION", "software");
		void (*x_error_handler)(void *d, void *e) = [](void *d, void *e) -> void
		{
			Log("X11 Error!\n");
			bErrored = true;
		};
		void *ptr = SDL_LoadObject("libX11.so");
		if(ptr)
		{
			int (*pXSetErrorHandler)(void (*handler)(void *d, void *e));
			*(void**)&pXSetErrorHandler = SDL_LoadFunction(ptr, "XSetErrorHandler");
			if(pXSetErrorHandler)
				pXSetErrorHandler(x_error_handler);
		}
	}
	int renderer_index = -1;
	unsigned int flags = SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED;

	// OpenGL implementations are bloated
	// Even with LIBGL_ALWAYS_INDIRECT or something it loads llvm, libdrm_* for all known and unknown gpus, etc..
	// try set SDL not even try to load glX, EGL
	// todo: SDL3 has vulkan renderer, which may get much less overhead
	if(preferSoftware)
	{
		// force only software renderer.
		// if it's unavailiable, will fallback to gl1 anyway
		// todo: what about GLES-only systems? Local GL implementation may work on ES1
		// GLES2 SDL_Rendererer is known to be the slowest
		SDL_SetHint("SDL_FRAMEBUFFER_ACCELERATION", "software");
		SDL_SetHint("SDL_RENDER_DRIVER", "software");
		int count = SDL_GetNumRenderDrivers();
		for(int i = 0; i < count; i++)
		{
			SDL_RendererInfo info;
			SDL_GetRenderDriverInfo(i, &info);
			if(info.flags & SDL_RENDERER_SOFTWARE)
			{
				renderer_index = i;
				flags &= ~SDL_RENDERER_ACCELERATED;
				flags |= SDL_RENDERER_SOFTWARE;
				break;
			}
		}
	}

	gPlatformState.renderer = SDL_CreateRenderer(gPlatformState.window, renderer_index, flags);

	if (gPlatformState.renderer == nullptr)
	{
		Log("Error creating SDL_Renderer!");
		gPlatformState.useGL = true;
		SDL_DestroyWindow(gPlatformState.window);
		window_flags = (SDL_WindowFlags) (window_flags | SDL_WINDOW_OPENGL);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
		SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 1);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
		gPlatformState.window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, window_flags);
		gPlatformState.context = SDL_GL_CreateContext(gPlatformState.window);
		SDL_GL_MakeCurrent(gPlatformState.window, gPlatformState.context);
		SDL_GL_SetSwapInterval(1); // Enable vsync
		ImGui_ImplSDL2_InitForOpenGL(gPlatformState.window, gPlatformState.context);
		ImGui_ImplOpenGL2_Init();
		*(void**)&gPlatformState.pglClear = DYN_GL_GetProcAddress("glClear");
		*(void**)&gPlatformState.pglClearColor = DYN_GL_GetProcAddress("glClearColor");
		*(void**)&gPlatformState.pglViewport = DYN_GL_GetProcAddress("glViewport");
		*(void**)&gPlatformState.pglDisable = DYN_GL_GetProcAddress("glDisable");
		*(void**)&gPlatformState.pglEnable = DYN_GL_GetProcAddress("glEnable");
	}
	else
	{
		if(!preferSoftware && bErrored)
			Log("Warning: SDL_Renderer errored\n"
				"Try setting SDL_RENDER_DRIVER=software and SDL_FRAMEBUFFER_ACCELERATION=software if have some rendering issues!\n");

		ImGui_ImplSDL2_InitForSDLRenderer(gPlatformState.window, gPlatformState.renderer);
		ImGui_ImplSDLRenderer2_Init(gPlatformState.renderer);
	}
	return true;
}

void Platform_NewFrame()
{
	if(gPlatformState.useGL)
		ImGui_ImplOpenGL2_NewFrame();
	else
		ImGui_ImplSDLRenderer2_NewFrame();
	ImGui_ImplSDL2_NewFrame();
}

bool Platform_ProcessEvents(bool &hasEvents)
{
	SDL_Event event;
	hasEvents = false;
	while (SDL_PollEvent(&event))
	{
		hasEvents = true;
		ImGui_ImplSDL2_ProcessEvent(&event);
		if (event.type == SDL_QUIT)
			return false;
		if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(gPlatformState.window))
			return false;
		hasEvents = true;
	}
	return true;
}

void Platform_Present(ImGuiIO& io)
{
	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
	if(gPlatformState.useGL)
	{
		gPlatformState.pglDisable(GL_SCISSOR_TEST);
		gPlatformState.pglViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
		gPlatformState.pglClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
		gPlatformState.pglClear(GL_COLOR_BUFFER_BIT);
		gPlatformState.pglEnable(GL_SCISSOR_TEST);
		ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());
		SDL_GL_SwapWindow(gPlatformState.window);
	}
	else
	{
		SDL_RenderSetScale(gPlatformState.renderer, io.DisplayFramebufferScale.x, io.DisplayFramebufferScale.y);
		SDL_SetRenderDrawColor(gPlatformState.renderer, (Uint8)(clear_color.x * 255), (Uint8)(clear_color.y * 255), (Uint8)(clear_color.z * 255), (Uint8)(clear_color.w * 255));
		SDL_RenderClear(gPlatformState.renderer);
		ImGui_ImplSDLRenderer2_RenderDrawData(ImGui::GetDrawData());
		SDL_RenderPresent(gPlatformState.renderer);
	}
}
void Platform_Shutdown()
{
	if(gPlatformState.useGL)
	{
		ImGui_ImplOpenGL2_Shutdown();
		SDL_GL_DeleteContext(gPlatformState.context);
	}
	else
	{
		ImGui_ImplSDLRenderer2_Shutdown();
		SDL_DestroyRenderer(gPlatformState.renderer);
	}
	ImGui_ImplSDL2_Shutdown();
	SDL_DestroyWindow(gPlatformState.window);
	SDL_Quit();
}

#else
#error "Not implemented yet"
#endif

namespace ImGui {
	void TextUnformatted(const SubStr &s)
	{
		return TextUnformatted(s.begin, s.end);
	}
}

static void RunCommand(int pid, const Command &c);
static struct AppConsole
{
	char mWindowName[64];
	int mPid;
	bool show;
	char                  InputBuf[256];
	int mBufLen;
	ImVector<SubStr>       Items;
	SubStr InternalCommands[3] ={"help", "history", "clear"};
	ImVector<SubStr>       History;
	int                   HistoryPos;    // -1: new line, 0..History.Size-1 browsing history.
	ImGuiTextFilter       Filter;
	bool                  AutoScroll;
	bool                  ScrollToBottom;

	AppConsole()
	{
		ClearLog();
		memset(InputBuf, 0, sizeof(InputBuf));
		HistoryPos = -1;
		AutoScroll = true;
		ScrollToBottom = false;
	}
	~AppConsole()
	{
		ClearLog();
		for (int i = 0; i < History.Size; i++)
			History[i].Free();
	}

	void ClearLog()
	{
		for (int i = 0; i < Items.Size; i++)
			Items[i].Free();
		Items.clear();
	}

	template <size_t fmtlen, typename... Ts>
	void AddLog(const char (&fmt)[fmtlen], Ts... args ) //IM_FMTARGS(2)
	{
		char buf[512];
		int len = SBPrint(buf,fmt,args...);
		Items.push_back(SubStr(buf, len - 1).StrDup());
	}
	template <size_t fmtlen>
	void AddLog(const char (&fmt)[fmtlen])
	{
		Items.push_back(SubStr(fmt).StrDup());
	}

	void Draw()
	{
		if(!mWindowName[0])
		{
			show = false;
			return;
		}
		ImGui::SetNextWindowSize(ImVec2(520, 600), ImGuiCond_FirstUseEver);
		if (!ImGui::Begin(mWindowName, &show))
		{
			ImGui::End();
			return;
		}

		// As a specific feature guaranteed by the library, after calling Begin() the last Item represent the title bar.
		// So e.g. IsItemHovered() will return true when hovering the title bar.
		// Here we create a context menu only available from the title bar.
		if (ImGui::BeginPopupContextItem())
		{
			if (ImGui::MenuItem("Close Console"))
				show = false;
			ImGui::EndPopup();
		}

		if (ImGui::SmallButton("Clear"))           { ClearLog(); }
		ImGui::SameLine();
		bool copy_to_clipboard = ImGui::SmallButton("Copy");

		ImGui::Separator();

		// Options menu
		if (ImGui::BeginPopup("Options"))
		{
			ImGui::Checkbox("Auto-scroll", &AutoScroll);
			ImGui::EndPopup();
		}

		// Options, Filter
		if (ImGui::Button("Options"))
			ImGui::OpenPopup("Options");
		ImGui::SameLine();
		Filter.Draw("Filter (\"incl,-excl\") (\"error\")", 180);
		ImGui::Separator();

		// Reserve enough left-over height for 1 separator + 1 input text
		const float footer_height_to_reserve = ImGui::GetStyle().ItemSpacing.y + ImGui::GetFrameHeightWithSpacing();
		if (ImGui::BeginChild("ScrollingRegion", ImVec2(0, -footer_height_to_reserve), ImGuiChildFlags_None, ImGuiWindowFlags_HorizontalScrollbar))
		{
			if (ImGui::BeginPopupContextWindow())
			{
				if (ImGui::Selectable("Clear")) ClearLog();
				ImGui::EndPopup();
			}
			ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 1)); // Tighten spacing
			if (copy_to_clipboard)
				ImGui::LogToClipboard();
			for (const SubStr &item : Items)
			{
				if (!Filter.PassFilter(item.begin, item.end))
					continue;

				// Normally you would store more information in your item than just a string.
				// (e.g. make Items[] an array of structure, store color/type etc.)
				ImVec4 color;
				bool has_color = false;
				if(item.StartsWith("[error]")){ color = ImVec4(1.0f, 0.4f, 0.4f, 1.0f); has_color = true; }
				else if (item.StartsWith("# ")) { color = ImVec4(1.0f, 0.8f, 0.6f, 1.0f); has_color = true; }
				if (has_color)
					ImGui::PushStyleColor(ImGuiCol_Text, color);
				ImGui::TextUnformatted(item.begin, item.end);
				if (has_color)
					ImGui::PopStyleColor();
			}
			if (copy_to_clipboard)
				ImGui::LogFinish();

			// Keep up at the bottom of the scroll region if we were already at the bottom at the beginning of the frame.
			// Using a scrollbar or mouse-wheel will take away from the bottom edge.
			if (ScrollToBottom || (AutoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY()))
				ImGui::SetScrollHereY(1.0f);
			ScrollToBottom = false;

			ImGui::PopStyleVar();
		}
		ImGui::EndChild();
		ImGui::Separator();

		// Command-line
		bool reclaim_focus = false;
		ImGuiInputTextFlags input_text_flags = ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_EscapeClearsAll | ImGuiInputTextFlags_CallbackCompletion | ImGuiInputTextFlags_CallbackHistory | ImGuiInputTextFlags_CallbackEdit;
		if (ImGui::InputText("Input", InputBuf, IM_ARRAYSIZE(InputBuf), input_text_flags, &TextEditCallbackStub, (void*)this))
		{
			SubStr buf = SubStr(InputBuf, mBufLen);
			char* s = InputBuf;
			while(buf.end > buf.begin && (*(buf.end - 1) == ' ') )
					buf.end--;
			if (s[0])
				ExecCommand(buf);
			mBufLen = 0;
			InputBuf[0] = 0;
			reclaim_focus = true;
		}

		// Auto-focus on window apparition
		ImGui::SetItemDefaultFocus();
		if (reclaim_focus)
			ImGui::SetKeyboardFocusHere(-1); // Auto focus previous widget

		ImGui::End();
	}

	void    ExecCommand(const SubStr &command_line)
	{
		AddLog("# %s\n", command_line);

		// Insert into history. First find match and delete it so it can be pushed to the back.
		// This isn't trying to be smart or optimal.
		HistoryPos = -1;
		for (int i = History.Size - 1; i >= 0; i--)
			if (History[i].Equals(command_line))
			{
				History[i].Free();
				History.erase(History.begin() + i);
				break;
			}
		History.push_back(command_line.StrDup());

		// Process command
		if (command_line.Equals("clear"))
		{
			ClearLog();
		}
		else if (command_line.Equals("help"))
		{
			AddLog("Internal Commands:");
			for (int i = 0; i < IM_ARRAYSIZE(InternalCommands); i++)
				AddLog("- %s", InternalCommands[i]);
			AddLog("Client Commands:");
			for (int i = 1; i < IM_ARRAYSIZE(gCommands); i++)
				AddLog("- %s (%s) %s", gCommands[i].name, gCommands[i].sign, gCommands[i].description);
		}
		else if (command_line.Equals("history"))
		{
			int first = History.Size - 10;
			for (int i = first > 0 ? first : 0; i < History.Size; i++)
				AddLog("%3d: %s\n", i, History[i]);
		}
		else
		{
			Command c = Command(command_line);
			if(c.ctype != EVENT_POLL_NULL)
				RunCommand(mPid, c);
			else
				AddLog("Unknown command: '%s'\n", command_line);
		}

		// On command input, we scroll to bottom even if AutoScroll==false
		ScrollToBottom = true;
	}

	// In C++11 you'd be better off using lambdas for this sort of forwarding callbacks
	static int TextEditCallbackStub(ImGuiInputTextCallbackData* data)
	{
		AppConsole* console = (AppConsole*)data->UserData;
		return console->TextEditCallback(data);
	}

	int     TextEditCallback(ImGuiInputTextCallbackData* data)
	{
		//AddLog("cursor: %d, selection: %d-%d", data->CursorPos, data->SelectionStart, data->SelectionEnd);
		switch (data->EventFlag)
		{
		case ImGuiInputTextFlags_CallbackCompletion:
			{
				// Example of TEXT COMPLETION

				// Locate beginning of current word
				const char* word_end = data->Buf + data->CursorPos;
				const char* word_start = word_end;
				while (word_start > data->Buf)
				{
					const char c = word_start[-1];
					if (c == ' ' || c == '\t' || c == ',' || c == ';')
						break;
					word_start--;
				}

				if(word_start != data->Buf)
				{
					break;
				}

				// Build a list of candidates
				ImVector<SubStr> candidates;
				SubStr word = SubStr{word_start, word_end};
				for (int i = 1; i < IM_ARRAYSIZE(gCommands); i++)
					if(gCommands[i].name.StartsWith(word))
						candidates.push_back(gCommands[i].name);
				for (int i = 0; i < IM_ARRAYSIZE(InternalCommands); i++)
					if(InternalCommands[i].StartsWith(word))
						candidates.push_back(InternalCommands[i]);

				if (candidates.Size == 0)
				{
					// No match
					AddLog("No match for \"%s\"!\n", word);
				}
				else if (candidates.Size == 1)
				{
					// Single match. Delete the beginning of the word and replace it entirely so we've got nice casing.
					data->DeleteChars((int)(word_start - data->Buf), (int)(word_end - word_start));
					data->InsertChars(data->CursorPos, candidates[0].begin, candidates[0].end);
					data->InsertChars(data->CursorPos, " ");
				}
				else
				{
					// Multiple matches. Complete as much as we can..
					// So inputing "C"+Tab will complete to "CL" then display "CLEAR" and "CLASSIFY" as matches.
					int match_len = (int)(word_end - word_start);
					for (;;)
					{
						int c = 0;
						bool all_candidates_matches = true;
						for (int i = 0; i < candidates.Size && all_candidates_matches; i++)
							if (i == 0)
								c = candidates[i].begin[match_len];
							else if (c == 0 || c != candidates[i].begin[match_len])
								all_candidates_matches = false;
						if (!all_candidates_matches)
							break;
						match_len++;
					}

					if (match_len > 0)
					{
						data->DeleteChars((int)(word_start - data->Buf), (int)(word_end - word_start));
						data->InsertChars(data->CursorPos, candidates[0].begin, candidates[0].begin + match_len);
					}

					// List matches
					AddLog("Possible matches:\n");
					for (int i = 0; i < candidates.Size; i++)
						AddLog("- %s\n", candidates[i]);
				}

				break;
			}
		case ImGuiInputTextFlags_CallbackHistory:
			{
				// Example of HISTORY
				const int prev_history_pos = HistoryPos;
				if (data->EventKey == ImGuiKey_UpArrow)
				{
					if (HistoryPos == -1)
						HistoryPos = History.Size - 1;
					else if (HistoryPos > 0)
						HistoryPos--;
				}
				else if (data->EventKey == ImGuiKey_DownArrow)
				{
					if (HistoryPos != -1)
						if (++HistoryPos >= History.Size)
							HistoryPos = -1;
				}

				// A better implementation would preserve the data on the current input line along with cursor position.
				if (prev_history_pos != HistoryPos)
				{
					const char* history_str = (HistoryPos >= 0) ? History[HistoryPos].begin : "";
					data->DeleteChars(0, data->BufTextLen);
					data->InsertChars(0, history_str);
				}
			}
		}

		mBufLen = data->BufTextLen;
		return 0;
	}
} gConsole;


template <typename Type, const auto &NAME, size_t nlen>
struct StructField_ : Type
{
	constexpr static const char *name = NAME;
	StructField_() = default;
	StructField_(const Type &t){ *(Type*)this = t;}
	template <typename DumperType>
	StructField_(Dumper<DumperType> &l){
		auto &vv = l.GetData(this);
		DumperType d2 = l.d;
		if( l.d.show)
		{
			d2.tname = name;
			DumpNamedStruct(d2, (Type*)&vv);
		}

	}
};

#define StructField(Type, name) \
constexpr static const char fld_name_##name[] = #name; \
	StructField_<Type, fld_name_##name, sizeof(fld_name_##name) - 1> name


#define HashMapField(Key, Value, name) \
constexpr static const char fld_name_##name[] = #name; \
	HashMapField_<Key, Value, fld_name_##name, sizeof(fld_name_##name) - 1> name

void NameForKey(char (&keyname)[128], unsigned long long key)
{
	SBPrint(keyname, "%16x", key);
}

void NameForKey(char (&keyname)[128], const SubStr &key)
{
	SBPrint(keyname, "%s", key);
}

template <typename Key, typename Value, const auto &NAME, size_t nlen>
struct HashMapField_ : HashMap<Key, Value>
{
	constexpr static const char *name = NAME;
	HashMapField_() = default;
	template <typename DumperType>
	HashMapField_(Dumper<DumperType> &l){
		auto &vv = l.GetData(this);
		DumperType d2 = l.d;
		d2.startOpen = false;

		if(l.d.show)
		{
			d2.tname = name;
			d2.Begin();
			if(d2.show)
			{
				DumperType d3 = l.d;
				HASHMAP_FOREACH(vv, node)
				{
					char keyname[128];
					NameForKey(keyname,node->k);
					d3.tname = keyname;

					DumpNamedStruct(d3, &node->v);
				}
			}
			d2.End();
		}

	}
};

struct AppActionState
{
	StructField(AppAction,mState);
	HashMapField(int, AppBinding, bindings);
};

struct AppActionSetState
{
	StructField(AppActionSet,mState);
	HashMapField(SubStr,AppActionState,mActions);
};

struct AppState;
struct AppSessionState
{
	AppSession mState;
	HashMap<SubStr,AppActionSetState> mActionsSets;
	HashMap<SubStr,AppSource> mSources[USER_PATH_COUNT + 1];
	HashMap<unsigned long long, AppActionMap> mActionMaps[USER_PATH_COUNT];
	HashMap<SubStr, AppRPN> mExpressions;
	HashMap<int, AppCustomAction> mCustomActions;
	HashMap<unsigned int, AppSource*> mSourceIndexes[4];
	HashMap<SubStr, AppSource*> mExternalSources;
	HashMap<int, AppRPN*> mExpressionIndexes;
	SubStr mMappingToolSelectedAction = "Select action";
	SubStr mMappingToolSelectedTarget = "Select source";
	AppActionState *mMappingToolSelectedActionPtr = nullptr;
	AppActionState *mMappingToolSelectedTargetPtr = nullptr;
	int mMappingToolSelectedMode = 0, mMappingToolSourceHand = 0, mMappingToolActionHand = USER_INVALID, mMappingToolActionAxis = 0, mMappingToolInputMode;
	char mActionNameInput[32];
	char mExpressionInput[64];

	AppState *pState = nullptr;
	bool show;
	char mSessionName[32];
	void SetName(AppState *state, int pid, unsigned long long handle)
	{
		if(mSessionName[0])
			return;
		pState = state;
		SBPrint(mSessionName, "Session %x (%d)", handle, pid);
		show = true;
	}
};

enum AppStatus
{
	AppRunning,
	AppStale,
	AppExited,
	AppTerminated
};

struct GraphData
{
	CycleArray<float,8> values;
	size_t counter = 0;
};

struct AppGraphs
{
	char mPlotName[64];
	HashMap<SubStr, GraphData> mGraphs;
	bool showVars = 0;
	bool showSources = 0;
	bool showActMaps = 0;
	bool show = 0;
	unsigned int cur_mask = 0;
	float curInterval = 0.2f;
	bool autoUpdate;
};

struct AppState
{
	AppConsole mConsole;
	int pid;
	AppReg mReg;
	HashMap<unsigned long long, AppSessionState> mSessions;
	HashMap<SubStr, AppVar> mVariables;
	AppGraphs mPlots;
	char mAppName[64];
	char mVarName[64];
	bool showVars;
	unsigned long long mLastSend = 0, mLastReceive = 0;
	AppStatus mState;
	AppSessionState *pActiveSession;

	void SetState(const char *name, unsigned int pidKey, AppStatus s)
	{
		SubStr st = "";
		if(s == AppStale)
			st = "[Stale]";
		if(s == AppExited)
			st = "[Exited]";
		if(s == AppTerminated)
			st = "[Terminated]";
		SBPrint(mConsole.mWindowName, "%s%s (%d) - Console###CON_%d", st,name, pid, pidKey);

		SBPrint(mAppName, "%s%s (%d)###APP_%d", st,name, pid, pidKey);
		SBPrint(mVarName, "%s%s (%d) - Variables###APPV_%d", st, name, pid, pidKey);
		SBPrint(mPlots.mPlotName, "%s%s (%d) - Plots###APPP_%d", st, name, pid, pidKey);
		mState = s;

		mConsole.mPid = pid;
	}
};

static void AddPlotPoint(AppState &s, const SubStr key, unsigned long long time, float val)
{
	// todo: need own time-based plot implementation
	s.mPlots.mGraphs[key].values[s.mPlots.mGraphs[key].counter++] = val;
}

static HashMap<int, AppState> gApps;

struct EventDumper
{
	AppConsole &console;
	const char *tname;
	void Begin(){}
	void Dump(const SubStr &name, const SubStr &val)
	{
		console.AddLog("received %s: %s %s\n", tname, name, val);
	}
	void End(){}
};


struct HandleConsole
{
	AppState *state;
	template <typename T>
	void Handle(const T &packet) const
	{
		EventDumper d{state? state->mConsole : gConsole,TypeName<T>()};
		DumpNamedStruct(d, &packet);
	}
};


struct HandleApp
{
	int pid;
	AppState &state;
	template <typename T>
	void Handle(const T &packet) const{}
	void Handle(const AppSession &session) const
	{
		AppSessionState &s = state.mSessions[session.handle];
		s.mState = session;
		s.SetName(&state, pid, session.handle);
		if(session.state == (unsigned int)(-1) || session.state >= 1 && session.state <= 5)
			state.pActiveSession = &s;
		else if(session.state == SESSOION_DESTROY && state.pActiveSession->mState.handle == session.handle)
			state.pActiveSession = nullptr;
	}
	void Handle(const AppActionSet &set) const
	{
		state.mSessions[set.session.val].mActionsSets[SubStrB(set.setName.val)].mState = set;
		state.mSessions[set.session.val].SetName(&state, pid, set.session.val);
	}
	void Handle(const AppAction &act) const
	{
		state.mSessions[act.session.val].mActionsSets[SubStrB(act.setName.val)].mActions[SubStrB(act.actName.val)].mState = act;
		state.mSessions[act.session.val].SetName(&state, pid, act.session.val);
	}
	void Handle(const AppBinding &act) const
	{
		state.mSessions[act.session.val].mActionsSets[SubStrB(act.setName.val)].mActions[SubStrB(act.actName.val)].bindings[act.index] = act;
		state.mSessions[act.session.val].SetName(&state, pid, act.session.val);
	}
	void Handle(const AppVar &var) const
	{
		state.mVariables[SubStrB(var.name.val)] = var;
		char buf[32];
		SBPrint(buf, "var.%s",var.name.val);
		AddPlotPoint(state, SubStrB(buf), var.time, var.value.val);
	}
	void Handle(const AppSource &src) const
	{
		SubStr name = SubStrB(src.name.val);
		AppSource &ref = state.mSessions[src.session.val].mSources[src.hand][name];
		ref = src;
		state.mSessions[src.session.val].SetName(&state, pid, src.session.val);
		if(src.stype.val > 0 && src.stype.val < 5)
			state.mSessions[src.session.val].mSourceIndexes[src.stype.val - 1][src.index | src.hand << 16] = &ref;
		if(src.stype.val == 4)
			state.mSessions[src.session.val].mExternalSources[name] = &ref;

		if(src.isActive)
		{
			char buf[32];
			SBPrint(buf, "src.%s[%d].x",src.name.val, src.hand.val);
			AddPlotPoint(state, SubStrB(buf), src.time, src.x.val);
			if(src.stype == 3) // vector2
			{
				SBPrint(buf, "src.%s[%d].y",src.name.val, src.hand.val);
				AddPlotPoint(state, SubStrB(buf), src.time, src.y.val);
			}
		}
	}
	void Handle(const AppActionMap &map) const
	{
		state.mSessions[map.session.val].mActionMaps[map.hand][map.handle] = map;
		state.mSessions[map.session.val].SetName(&state, pid, map.session.val);
		if(map.override && ((map.mapIndex >= 0) || map.hasAxisMapping) )
		{
			char buf[256];
			SBPrint(buf, "map.%s[%d].x",map.actName.val, map.hand.val);
			AddPlotPoint(state, SubStrB(buf), map.time, map.x.val);
			if(map.actType == 3) // vector2
			{
				SBPrint(buf, "map.%s[%d].y",map.actName.val, map.hand.val);
				AddPlotPoint(state, SubStrB(buf), map.time, map.y.val);
			}
		}
	}
	void Handle(const AppRPN &rpn) const
	{
		AppRPN &ref = state.mSessions[rpn.session.val].mExpressions[SubStrB(rpn.source.val)];
		ref = rpn;
		state.mSessions[rpn.session.val].SetName(&state, pid, rpn.session.val);
		if(rpn.index >= 0)
			state.mSessions[rpn.session.val].mExpressionIndexes[rpn.index] = &ref;
	}
	void Handle(const AppCustomAction &act) const
	{
		state.mSessions[act.session.val].mCustomActions[act.index] = act;
		state.mSessions[act.session.val].SetName(&state, pid, act.session.val);
	}
};


struct ImGuiTreeDumper
{
	const char *tname;
	bool show;
	void Begin()
	{
		show = ImGui::TreeNode(tname);
	}
	bool Dump(const SubStr &name, const SubStr &val)
	{
		if(!show)
			return false;
		ImGui::TextUnformatted(name);
		ImGui::SameLine();
		ImGui::TextUnformatted(val);
		return true;
	}
	void End()
	{
		if(show)
		ImGui::TreePop();
	}
};

struct ImGuiTableTreeDumper
{
	const char *tname;
	bool show;
	bool startOpen = false;
	void Begin()
	{
		ImGui::TableNextRow();
		ImGui::TableSetColumnIndex(1);
		ImGui::TextUnformatted("<object>");
		ImGui::TableSetColumnIndex(0);
		if(startOpen)
			ImGui::SetNextItemOpen(true, ImGuiCond_Once);
		show = ImGui::TreeNode(tname);
	}
	bool Dump(const SubStr &name, const SubStr &val)
	{
		if(!show)
			return false;
		ImGui::TableNextRow();
		ImGui::TableSetColumnIndex(0);
		ImGui::TreeNodeEx(name.begin, ImGuiTreeNodeFlags_SpanAllColumns | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_Bullet);
		ImGui::TableSetColumnIndex(1);
		ImGui::TextUnformatted(val);
		return true;
	}
	void End()
	{
		if(show)
			ImGui::TreePop();
	}
};

static unsigned int gRequestFrames = 3;
static struct Client
{
	int fd  = -1;
	bool Connect(int port)
	{
		if(fd >= 0)
			close(fd);
		fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		sockaddr_in addr;
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
		addr.sin_port = htons(port);
		connect(fd, (sockaddr*)&addr, sizeof(addr));
		ClientReg reg = {true};
		Send(reg);
		return true;
	}
	template <typename T>
	int Send(const T &data, unsigned char target = 0xF, pid_t targetPid = 0)
	{
		if(fd < 0)
			return -1;
		EventPacket p;
		p.head.sourcePid = getpid();
		SubStr("client_simple").CopyTo(p.head.displayName);
		p.head.target = target;
		p.head.targetPid = targetPid;
		p.head.type = T::type;
		memcpy(&p.data, &data, sizeof(data));
		return send(fd,&p, ((char*)&p.data - (char*)&p) + sizeof(data), 0);
	}
	template <typename T>
	void Send(const T &data, AppState &s)
	{
		s.mLastSend = GetTimeU64();
		Send(data, TARGET_APP, s.pid);
	}
	void RunFrame(unsigned int time)
	{
		if(fd < 0)
		{
			usleep(time);
			return;
		}
		EventPacket p;
		struct timeval tv;
		fd_set rfds;
		tv.tv_sec = 0;
		tv.tv_usec = time;
		FD_ZERO( &rfds );
		FD_SET(fd, &rfds);
		if( select( fd + 1, &rfds, NULL, NULL,&tv ) > 0)
		{
			while(true)
			{
				if(!(FD_ISSET(fd,&rfds) && (recv(fd, &p, sizeof(p), MSG_DONTWAIT) >= 0)))
					return;
				gRequestFrames = 4;
				unsigned int pidKey = p.head.sourcePid + ((unsigned int)p.head.instance) << 30;
				if(p.head.type == EVENT_APP_REGISTER)
				{
					AppState &s = gApps[pidKey];
					s.pid = p.head.sourcePid;
					s.SetState(p.head.displayName, pidKey, AppRunning);
					if(!s.mReg.name.val[0] && p.data.appReg.name.val[0])
						s.mReg = p.data.appReg;
					if(!s.mReg.name.val[0])
						SubStrB(p.head.displayName).CopyTo(s.mReg.name.val);
				}

				AppState *s = gApps.GetPtr(pidKey);
				if(p.head.target & TARGET_CLI && p.head.type != EVENT_APP_REGISTER)
				{
					HandleConsole h{s};
					HandlePacket(h, p);
				}
				if(s)
				{
					s->mLastReceive = GetTimeU64();
					if(p.head.type == EVENT_APP_DESTROY)
					{
						s->SetState(p.head.displayName, pidKey, p.data.destroy.calledFromXR?AppExited:AppTerminated);
					}
					HandleApp a{p.head.sourcePid,*s};
					HandlePacket(a, p);
				}
			}
		}
	}
} gClient;

static void RunCommand(int pid, const Command &c)
{
	gClient.Send(c,TARGET_APP, pid);
}
constexpr const char *gUserDevNames[] =
{
	"Left",
	"Right",
	"Head",
	"Gamepad",
	"Auto",
	"External"
};

#define SLEEP_ACTIVE 16666666
#define SLEEP_SUBACTIVE 2500000
#define SLEEP_IDLE  100000000
constexpr static long long sleepTimes[4]
{
	50000000,
	16666666,
	16666666,
	10000000,
};

void FrameControl()
{
	static unsigned long long lastTime;
	unsigned long long time = GetTimeU64();
	long long timeDiff = sleepTimes[gRequestFrames] - (time - lastTime);
	if(timeDiff <= 5000000)
		timeDiff = 0;
	gClient.RunFrame(timeDiff / 1000);
	lastTime = time;
}

void DrawActionSetTab(AppState &ast, AppSessionState &s)
{
	if(ImGui::Button("Get Application sets"))
	{
		Command cmd;
		cmd.ctype = EVENT_POLL_DUMP_APP_BINDINGS;
		cmd.datasize = 0;
		cmd.gui = true;
		gClient.Send(cmd, ast);
	}
	ImGui::SameLine();
	if(ImGui::Button("Get Layer sets"))
	{
		Command cmd;
		cmd.ctype = EVENT_POLL_DUMP_LAYER_BINDINGS;
		cmd.datasize = 0;
		cmd.gui = true;
		gClient.Send(cmd, ast);
	}
	HASHMAP_FOREACH(s.mActionsSets, as)
	{
		ImGui::SetNextItemOpen(true, ImGuiCond_Once);
		if(ImGui::CollapsingHeader(as->k.begin))
		{
			ImGui::PushID(as->k.begin);
			if (ImGui::BeginTable("##split", 2, ImGuiTableFlags_Resizable | ImGuiTableFlags_BordersOuter| ImGuiTableFlags_RowBg))
			{
				ImGui::TableSetupScrollFreeze(0, 1);
				ImGui::TableSetupColumn("Object");
				ImGui::TableSetupColumn("Contents");
				ImGui::TableHeadersRow();
				ImGuiTableTreeDumper d{"Action set info"};
				d.startOpen = true;
				DumpNamedStruct(d, &as->v);
				ImGui::EndTable();
			}
			ImGui::PopID();
		}
	}
}

void DrawSourcesTab(AppState &as, AppSessionState &s)
{
	if(ImGui::Button("Update"))
	{
		Command cmd;
		cmd.ctype = EVENT_POLL_DUMP_SOURCES;
		cmd.datasize = 0;
		cmd.gui = true;
		gClient.Send(cmd, as);
	}
	//ImGui::SetNextItemOpen(true, ImGuiCond_Once);
	//if(ImGui::CollapsingHeader("Sources dump"))
	{
		if (ImGui::BeginTable("##split", 2, ImGuiTableFlags_Resizable | ImGuiTableFlags_BordersOuter| ImGuiTableFlags_RowBg | ImGuiTableFlags_ScrollY))
		{
			ImGui::TableSetupScrollFreeze(0, 1);
			ImGui::TableSetupColumn("Object");
			ImGui::TableSetupColumn("Contents");
			ImGui::TableHeadersRow();

			for(int i = 0; i < USER_PATH_COUNT + 1; i++)
			{
				ImGuiTableTreeDumper d{gUserDevNames[i]};
				if(s.mSources[i].Begin().n == nullptr)
					continue;
				d.startOpen = true;
				d.Begin();
				if(d.show)
				HASHMAP_FOREACH(s.mSources[i], src)
				{
					char key[128];
					ImGuiTableTreeDumper d2{key};
					NameForKey(key, src->k);

					DumpNamedStruct(d2, &src->v);
				}
				d.End();
			}

			ImGui::EndTable();
		}
	}
}

void DrawActionMapsTab(AppState &as, AppSessionState &s)
{
	if(ImGui::Button("Update"))
	{
		Command cmd;
		cmd.ctype = EVENT_POLL_DUMP_ACTION_MAPS;
		cmd.datasize = 0;
		cmd.gui = true;
		gClient.Send(cmd, as);
	}
	//ImGui::SetNextItemOpen(true, ImGuiCond_Once);
	//if(ImGui::CollapsingHeader("Action maps dump"))
	{
		if (ImGui::BeginTable("##split", 2, ImGuiTableFlags_Resizable | ImGuiTableFlags_BordersOuter| ImGuiTableFlags_RowBg | ImGuiTableFlags_ScrollY))
		{
			ImGui::TableSetupScrollFreeze(0, 1);
			ImGui::TableSetupColumn("Object");
			ImGui::TableSetupColumn("Contents");
			ImGui::TableHeadersRow();

			for(int i = 0; i < USER_PATH_COUNT; i++)
			{
				ImGuiTableTreeDumper d{gUserDevNames[i]};
				d.startOpen = true;
				d.Begin();
				if(d.show)
				HASHMAP_FOREACH(s.mActionMaps[i], src)
				{
					char key[128];
					ImGuiTableTreeDumper d2{key};
					SBPrint(key, "%s##%x", src->v.actName.val, src->k);
					d2.startOpen = src->v.override;
					DumpNamedStruct(d2, &src->v);
				}
				d.End();
			}

			ImGui::EndTable();
		}
	}
}


void DrawExpressionsTab(AppState &as, AppSessionState &s)
{
	if(ImGui::Button("Update"))
	{
		Command cmd;
		cmd.ctype = EVENT_POLL_DUMP_EXPRESSIONS;
		cmd.datasize = 0;
		cmd.gui = true;
		gClient.Send(cmd, as);
	}

	if (ImGui::BeginTable("##split", 2, ImGuiTableFlags_Resizable | ImGuiTableFlags_BordersOuter| ImGuiTableFlags_RowBg | ImGuiTableFlags_ScrollY))
	{
		ImGui::TableSetupScrollFreeze(0, 1);
		ImGui::TableSetupColumn("Object");
		ImGui::TableSetupColumn("Contents");
		ImGui::TableHeadersRow();


		HASHMAP_FOREACH(s.mExpressions, src)
		{
			ImGuiTableTreeDumper d2{src->k.begin};
			d2.startOpen = true;
			DumpNamedStruct(d2, &src->v);
		}

		ImGui::EndTable();
	}
}

void DrawCustomActionsTab(AppState &as, AppSessionState &s)
{
	if(ImGui::Button("Update"))
	{
		Command cmd;
		cmd.ctype = EVENT_POLL_DUMP_CUSTOM_ACTIONS;
		cmd.datasize = 0;
		cmd.gui = true;
		gClient.Send(cmd, as);
	}

	if (ImGui::BeginTable("##split", 2, ImGuiTableFlags_Resizable | ImGuiTableFlags_BordersOuter| ImGuiTableFlags_RowBg | ImGuiTableFlags_ScrollY))
	{
		ImGui::TableSetupScrollFreeze(0, 1);
		ImGui::TableSetupColumn("Object");
		ImGui::TableSetupColumn("Contents");
		ImGui::TableHeadersRow();


		HASHMAP_FOREACH(s.mCustomActions, src)
		{
			char key[128];
			NameForKey(key, src->k);
			ImGuiTableTreeDumper d2{key};
			d2.startOpen = true;
			DumpNamedStruct(d2, &src->v);
		}

		ImGui::EndTable();
	}
}
static constexpr const char *const gszUserMappingSuffixes[] =
{
		".left",
		".right",
		".head",
		".gamepad",
		"",
		""
};

void DrawCreateMapping(AppState &as, AppSessionState &s)
{
	if(!ImGui::BeginTable("##add_mapping", 2))
		return;
	ImGui::TableSetupColumn("key", ImGuiTableColumnFlags_WidthFixed);
	ImGui::TableSetupColumn("value", ImGuiTableColumnFlags_WidthStretch);
	ImGui::TableNextRow();
	ImGui::TableSetColumnIndex(0);
	ImGui::TextUnformatted("Map action");
	ImGui::SetItemTooltip("Action list got from application");
	ImGui::TableSetColumnIndex(1);
	ImGui::SetNextItemWidth(-ImGui::GetContentRegionAvail().x * 0.3);
	bool red = !s.mMappingToolSelectedActionPtr || (s.mMappingToolActionHand != USER_INVALID && !(s.mMappingToolSelectedActionPtr->mState.subactionMask & 1U << s.mMappingToolActionHand));
	if(red)
		ImGui::PushStyleColor(ImGuiCol_FrameBg,IM_COL32(255,85,85,80));
	char config_example[1024];
	char command_example[256];

	if(ImGui::BeginCombo("###Map action", s.mMappingToolSelectedAction.begin))
	{
		bool has_actions = false;
		HASHMAP_FOREACH(s.mActionsSets, set)
		{
			if(set->k.Equals("layer_action_set"))
				continue;
			HASHMAP_FOREACH(set->v.mActions, act)
			{
				if(ImGui::Selectable(act->k.begin))
				{
					s.mMappingToolSelectedAction = act->k;
					s.mMappingToolSelectedActionPtr = &act->v;
					s.mMappingToolSelectedTarget = "Select source";
					s.mMappingToolSelectedTargetPtr = nullptr;
				}
				has_actions = true;
			}
		}
		if(!has_actions && ImGui::Selectable("Update actions"))
		{
			Command cmd;
			cmd.ctype = EVENT_POLL_DUMP_APP_BINDINGS;
			cmd.datasize = 0;
			cmd.gui = true;
			gClient.Send(cmd, as);
		}
		if(!has_actions)
			ImGui::SetItemTooltip("Select again");
		ImGui::EndCombo();
	}
	ImGui::SameLine();
	ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);

	//ImGui::Combo("###Action controller", &s.mMappingToolActionHand, "Left\0Right\0Head\0Gamepad\0Auto\0");
	if(ImGui::BeginCombo("##Action controller", gUserDevNames[s.mMappingToolActionHand]))
	{
		for(unsigned int i = 0; i < USER_INVALID; i++)
			if(s.mMappingToolSelectedActionPtr && s.mMappingToolSelectedActionPtr->mState.subactionMask & (1U << i))
				if(ImGui::Selectable(gUserDevNames[i]))
					s.mMappingToolActionHand = i;
		if(ImGui::Selectable("Auto"))
			s.mMappingToolActionHand = USER_INVALID;
		ImGui::EndCombo();
	}
	if(red)
		ImGui::PopStyleColor();
	if(!s.mMappingToolSelectedActionPtr)
	{
		ImGui::EndTable();
		return;
	}
	ImGui::TableNextRow();
	ImGui::TableSetColumnIndex(0);
	ImGui::TextUnformatted("Mapping mode");
	ImGui::SetItemTooltip("Select mapping mode\n\tDirect: copy data from runtime input\n\tConfig: load mapping from [actionmap.xxx] config section\n\tAdvanced: map random axis to random input or calculated value");
	ImGui::TableSetColumnIndex(1);
	ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
	red = !s.mMappingToolSelectedMode;
	if(red)
		ImGui::PushStyleColor(ImGuiCol_FrameBg,IM_COL32(255,85,85,80));
	if(ImGui::Combo("###Mapping mode", &s.mMappingToolSelectedMode, "Select\0Direct\0Config\0Advanced\0"))
	{
		s.mMappingToolSelectedTarget = "Select source";
		s.mMappingToolSelectedTargetPtr = nullptr;
	}
	if(red)
		ImGui::PopStyleColor();

	if(!s.mMappingToolSelectedMode)
	{
		ImGui::EndTable();
		return;
	}
	ImGui::TableNextRow();
	if(s.mMappingToolSelectedMode == 1)
	{
		ImGui::TableSetColumnIndex(0);
		ImGui::TextUnformatted("Select source");
		ImGui::SetItemTooltip("Source must be declared in config. See examples in source_templates dir\nFor Direct mode source type must match application action type");
		ImGui::TableSetColumnIndex(1);
		ImGui::SetNextItemWidth(-ImGui::GetContentRegionAvail().x * 0.3);
		red = !s.mMappingToolSelectedTargetPtr || (s.mMappingToolSourceHand != USER_INVALID && !(s.mMappingToolSelectedTargetPtr->mState.subactionMask & 1U << s.mMappingToolSourceHand));
		if(red)
			ImGui::PushStyleColor(ImGuiCol_FrameBg,IM_COL32(255,85,85,80));
		AppActionSetState *pSet = s.mActionsSets.GetPtr("layer_action_set");
		if(ImGui::BeginCombo("##Source", s.mMappingToolSelectedTarget.begin))
		{
			if(pSet)
			{
				HASHMAP_FOREACH(pSet->mActions, act)
				{
					if(act->v.mState.actionType == s.mMappingToolSelectedActionPtr->mState.actionType)
					{
						if(ImGui::Selectable(act->k.begin))
						{
							s.mMappingToolSelectedTarget = act->k;
							s.mMappingToolSelectedTargetPtr = &act->v;
						}
					}
				}
			}
			else
			{
				if(ImGui::Selectable("Update"))
				{
					Command cmd;
					cmd.ctype = EVENT_POLL_DUMP_LAYER_BINDINGS;
					cmd.datasize = 0;
					cmd.gui = true;
					gClient.Send(cmd, as);
				}
				ImGui::SetItemTooltip("Select again");
			}
			ImGui::EndCombo();
		}


		ImGui::SameLine();
		ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
		//ImGui::Combo("###Source controller", &s.mMappingToolSourceHand, "Left\0Right\0Head\0Gamepad\0Auto\0");
		if(ImGui::BeginCombo("##Source controller", gUserDevNames[s.mMappingToolSourceHand]))
		{
			for(unsigned int i = 0; i < USER_INVALID; i++)
				if(s.mMappingToolSelectedTargetPtr && s.mMappingToolSelectedTargetPtr->mState.subactionMask & (1U << i))
					if(ImGui::Selectable(gUserDevNames[i]))
						s.mMappingToolSourceHand = i;
			if(ImGui::Selectable("Auto"))
				s.mMappingToolSourceHand = USER_INVALID;
			ImGui::EndCombo();
		}
		if(red)
			ImGui::PopStyleColor();
		if(!red)
		{
			ImGui::TableNextRow();
			ImGui::TableSetColumnIndex(0);
			if(ImGui::Button("Apply",  ImVec2(ImGui::GetContentRegionAvail().x,0)))
			{
				Command cmd;
				char str[32];
				SBPrint(str, "%s%s", s.mMappingToolSelectedTarget, gszUserMappingSuffixes[s.mMappingToolSourceHand]);
				cmd.ctype = EVENT_POLL_MAP_DIRECT_SOURCE;
				cmd.datasize = 0;
				cmd.gui = true;
				cmd._AddStr(s.mMappingToolSelectedAction, 0);
				cmd.args[1].i = s.mMappingToolActionHand;
				cmd._AddStr(SubStrB(str), 2);
				gClient.Send(cmd, TARGET_APP, as.pid);
				if(cmd.args[1].i == 4)
				{
					cmd.args[1].i = 1;
					gClient.Send(cmd, TARGET_APP, as.pid);
					cmd.args[1].i = 0;
					gClient.Send(cmd, TARGET_APP, as.pid);
				}
			}
			ImGui::TableSetColumnIndex( 1 );
			SBPrint( command_example,  "%s %s %d %s%s", gCommands[EVENT_POLL_MAP_DIRECT_SOURCE].name.begin, s.mMappingToolSelectedAction.begin, s.mMappingToolActionHand, s.mMappingToolSelectedTarget.begin, gszUserMappingSuffixes[s.mMappingToolSourceHand] );

			if(ImGui::SmallButton("conf"))
				ImGui::OpenPopup(s.mSessionName);
			ImGui::SameLine();
			if(ImGui::SmallButton("copy"))
				ImGui::SetClipboardText(command_example);
			ImGui::SameLine();
			ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
			ImGui::InputText("##Command example", command_example, sizeof(command_example), ImGuiInputTextFlags_ReadOnly);

			if(ImGui::BeginPopup(s.mSessionName))
			{
				ImGui::PushStyleVar(ImGuiStyleVar_ChildBorderSize, 0);
				ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));
				if (ImGui::BeginChild("ResizableChild", ImVec2(300, ImGui::GetTextLineHeightWithSpacing() * 4), ImGuiChildFlags_Border | ImGuiChildFlags_ResizeY))
				{
					ImGui::TextUnformatted("Config example");
					SBPrint( config_example, "[actionmap.example_%s_%s]\nmap = %s%s\n[bindings.example]\n%s%s = example_%s_%s\n", s.mMappingToolSelectedAction.begin, gUserDevNames[s.mMappingToolActionHand], s.mMappingToolSelectedTarget.begin, gszUserMappingSuffixes[s.mMappingToolSourceHand], s.mMappingToolSelectedAction.begin, gszUserMappingSuffixes[s.mMappingToolActionHand], s.mMappingToolSelectedAction.begin, gUserDevNames[s.mMappingToolActionHand]);
					ImGui::InputTextMultiline("##Config example", config_example, sizeof(config_example), ImGui::GetContentRegionAvail(), ImGuiInputTextFlags_ReadOnly);
					ImGui::EndChild();
					if(ImGui::Button("Copy to Clipboard"))
						ImGui::SetClipboardText(config_example);
				}
				ImGui::PopStyleVar(2);
				ImGui::EndPopup();
			}
		}
	}
	if(s.mMappingToolSelectedMode == 2)
	{
		ImGui::TableSetColumnIndex(0);
		ImGui::TextUnformatted("Config section");
		ImGui::TableSetColumnIndex(1);
		red = !*s.mActionNameInput;
		if(red)
			ImGui::PushStyleColor(ImGuiCol_FrameBg,IM_COL32(255,85,85,80));ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
		ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
		ImGui::InputText("###Config section", s.mActionNameInput, sizeof(s.mActionNameInput));
		if(red)
			ImGui::PopStyleColor();
		if(*s.mActionNameInput)
		{
			ImGui::TableNextRow();
			ImGui::TableSetColumnIndex(0);
			if(ImGui::Button("Apply",  ImVec2(ImGui::GetContentRegionAvail().x,0)))
			{
				Command cmd;
				char str[32];
				SBPrint(str, "%s%s", s.mMappingToolSelectedTarget, gszUserMappingSuffixes[s.mMappingToolActionHand]);
				cmd.ctype = EVENT_POLL_MAP_ACTION;
				cmd.datasize = 0;
				cmd.gui = true;
				cmd._AddStr(s.mMappingToolSelectedAction, 0);
				cmd._AddStr(SubStrB(s.mActionNameInput), 1);
				cmd.args[2].i = s.mMappingToolActionHand;
				gClient.Send(cmd, TARGET_APP, as.pid);
				if(cmd.args[2].i == 4)
				{
					cmd.args[2].i = 1;
					gClient.Send(cmd, TARGET_APP, as.pid);
					cmd.args[2].i = 0;
					gClient.Send(cmd, TARGET_APP, as.pid);
				}
			}
			ImGui::TableSetColumnIndex( 1 );
			SBPrint( command_example, "%s %s %s %d", gCommands[EVENT_POLL_MAP_ACTION].name.begin, s.mMappingToolSelectedAction.begin, s.mActionNameInput, s.mMappingToolActionHand );

			if(ImGui::SmallButton("conf"))
				ImGui::OpenPopup(s.mSessionName);
			ImGui::SameLine();
			if(ImGui::SmallButton("copy"))
				ImGui::SetClipboardText(command_example);
			ImGui::SameLine();
			ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
			ImGui::InputText("##Command example", command_example, sizeof(command_example), ImGuiInputTextFlags_ReadOnly);

			if(ImGui::BeginPopup(s.mSessionName))
			{
				ImGui::PushStyleVar(ImGuiStyleVar_ChildBorderSize, 0);
				ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));
				if (ImGui::BeginChild("ResizableChild", ImVec2(300, ImGui::GetTextLineHeightWithSpacing() * 4), ImGuiChildFlags_Border | ImGuiChildFlags_ResizeY))
				{
					ImGui::TextUnformatted("Config example");
					SBPrint( config_example, "[bindings.example]\n%s%s = %s\n", s.mMappingToolSelectedAction.begin, gszUserMappingSuffixes[s.mMappingToolActionHand], s.mActionNameInput);
					ImGui::InputTextMultiline("##Config example", config_example, sizeof(config_example), ImGui::GetContentRegionAvail(), ImGuiInputTextFlags_ReadOnly);
					ImGui::EndChild();
					if(ImGui::Button("Copy to Clipboard"))
						ImGui::SetClipboardText(config_example);
				}
				ImGui::PopStyleVar(2);
				ImGui::EndPopup();
			}
		}
	}
	if(s.mMappingToolSelectedMode == 3)
	{
		ImGui::TableSetColumnIndex(0);
		ImGui::TextUnformatted("Axis");
		ImGui::SetItemTooltip("Select axis which you want to map");
		ImGui::TableSetColumnIndex(1);
		ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
		red = !s.mMappingToolActionAxis;
		if(red)
			ImGui::PushStyleColor(ImGuiCol_FrameBg,IM_COL32(255,85,85,80));
		ImGui::Combo("###Axis", &s.mMappingToolActionAxis, "Select axis\0axis1\0axis2\0");
		if(red)
			ImGui::PopStyleColor();
		if(s.mMappingToolActionAxis)
		{
			ImGui::TableNextRow();
			ImGui::TableSetColumnIndex(0);
			ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
			ImGui::PushStyleColor(ImGuiCol_FrameBg,ImVec4(0,0,0,0));
			ImGui::PushStyleColor(ImGuiCol_Button,ImVec4(0,0,0,0));
			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0,0));
			ImGui::Combo("###input mode", &s.mMappingToolInputMode, "Source\0Expression   \0", ImGuiComboFlags_WidthFitPreview);
			ImGui::SetItemTooltip("Source must be declared in config. See examples in source_templates dir\nYou may map axis to calculated expressions (TODO: document exprs)");
			ImGui::PopStyleColor(2);
			ImGui::PopStyleVar();
			ImGui::TableSetColumnIndex(1);
			if(s.mMappingToolInputMode == 0)
			{
				ImGui::SetNextItemWidth(-ImGui::GetContentRegionAvail().x * 0.3);
				red = !s.mMappingToolSelectedTargetPtr || (s.mMappingToolSourceHand != USER_INVALID && !(s.mMappingToolSelectedTargetPtr->mState.subactionMask & 1U << s.mMappingToolSourceHand));
				if(red)
					ImGui::PushStyleColor(ImGuiCol_FrameBg,IM_COL32(255,85,85,80));
				AppActionSetState *pSet = s.mActionsSets.GetPtr("layer_action_set");
				if(ImGui::BeginCombo("###Source", s.mMappingToolSelectedTarget.begin))
				{
					if(pSet)
					{
						HASHMAP_FOREACH(pSet->mActions, act)
						{
							if(ImGui::Selectable(act->k.begin))
							{
								s.mMappingToolSelectedTarget = act->k;
								s.mMappingToolSelectedTargetPtr = &act->v;
							}
						}
						HASHMAP_FOREACH(s.mExternalSources, ext)
						{
							if(ImGui::Selectable(ext->k.begin))
							{
								static AppActionState dummy;
								s.mMappingToolSelectedTarget = ext->k;
								s.mMappingToolSelectedTargetPtr = &dummy;
								dummy.mState.actionType = 4;
							}
						}

					}
					else
					{
						if(ImGui::Selectable("Update"))
						{
							Command cmd;
							cmd.ctype = EVENT_POLL_DUMP_LAYER_BINDINGS;
							cmd.datasize = 0;
							cmd.gui = true;
							gClient.Send(cmd, as);
							cmd.ctype = EVENT_POLL_DUMP_SOURCES;
							gClient.Send(cmd, as);
						}
						ImGui::SetItemTooltip("Select again");
					}
					ImGui::EndCombo();
				}
				ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
				//ImGui::Combo("###Source controller", &s.mMappingToolSourceHand, "Left\0Right\0Head\0Gamepad\0Auto\0");
				if(ImGui::BeginCombo("##Source controller", gUserDevNames[s.mMappingToolSourceHand]))
				{
					for(unsigned int i = 0; i < USER_INVALID; i++)
						if(s.mMappingToolSelectedTargetPtr && s.mMappingToolSelectedTargetPtr->mState.subactionMask & (1U << i))
							if(ImGui::Selectable(gUserDevNames[i]))
								s.mMappingToolSourceHand = i;
					if(ImGui::Selectable("Auto"))
						s.mMappingToolSourceHand = USER_INVALID;
					ImGui::EndCombo();
				}
				if(red)
					ImGui::PopStyleColor();
				if(!red)
				{
					ImGui::TableNextRow();
					ImGui::TableSetColumnIndex(0);
					if(ImGui::Button("Apply",  ImVec2(ImGui::GetContentRegionAvail().x,0)))
					{
						Command cmd;
						char str[32];
						SBPrint(str, "%s%s", s.mMappingToolSelectedTarget, gszUserMappingSuffixes[s.mMappingToolSourceHand]);
						cmd.ctype = EVENT_POLL_MAP_AXIS;
						cmd.datasize = 0;
						cmd.gui = true;
						cmd._AddStr(s.mMappingToolSelectedAction, 0);
						cmd.args[1].i = s.mMappingToolActionHand;
						cmd.args[2].i = s.mMappingToolActionAxis - 1;
						cmd._AddStr(SubStrB(str), 3);
						gClient.Send(cmd, TARGET_APP, as.pid);
						if(cmd.args[1].i == 4)
						{
							cmd.args[1].i = 1;
							gClient.Send(cmd, TARGET_APP, as.pid);
							cmd.args[1].i = 0;
							gClient.Send(cmd, TARGET_APP, as.pid);
						}
					}
					ImGui::TableSetColumnIndex( 1 );
					SBPrint( command_example,  "%s %s %d %d %s%s", gCommands[EVENT_POLL_MAP_AXIS].name.begin, s.mMappingToolSelectedAction.begin, s.mMappingToolActionHand, s.mMappingToolActionAxis - 1, s.mMappingToolSelectedTarget.begin, gszUserMappingSuffixes[s.mMappingToolSourceHand] );

					if(ImGui::SmallButton("conf"))
						ImGui::OpenPopup(s.mSessionName);
					ImGui::SameLine();
					if(ImGui::SmallButton("copy"))
						ImGui::SetClipboardText(command_example);
					ImGui::SameLine();
					ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
					ImGui::InputText("##Command example", command_example, sizeof(command_example), ImGuiInputTextFlags_ReadOnly);

					if(ImGui::BeginPopup(s.mSessionName))
					{
						ImGui::PushStyleVar(ImGuiStyleVar_ChildBorderSize, 0);
						ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));
						if (ImGui::BeginChild("ResizableChild", ImVec2(300, ImGui::GetTextLineHeightWithSpacing() * 4), ImGuiChildFlags_Border | ImGuiChildFlags_ResizeY))
						{
							ImGui::TextUnformatted("Config example");
							SBPrint( config_example, "[actionmap.example_%s_%s]\naxis%d = %s%s\n[bindings.example]\n%s%s = example_%s_%s\n", s.mMappingToolSelectedAction.begin, gUserDevNames[s.mMappingToolActionHand], s.mMappingToolActionAxis,s.mMappingToolSelectedTarget.begin, gszUserMappingSuffixes[s.mMappingToolSourceHand], s.mMappingToolSelectedAction.begin, gszUserMappingSuffixes[s.mMappingToolActionHand], s.mMappingToolSelectedAction.begin, gUserDevNames[s.mMappingToolActionHand]);
							ImGui::InputTextMultiline("##Config example", config_example, sizeof(config_example), ImGui::GetContentRegionAvail(), ImGuiInputTextFlags_ReadOnly);
							ImGui::EndChild();
							if(ImGui::Button("Copy to Clipboard"))
								ImGui::SetClipboardText(config_example);
						}
						ImGui::PopStyleVar(2);
						ImGui::EndPopup();
					}
				}
			}
			else
			{
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
				bool red = !*s.mExpressionInput;
				if(red)
					ImGui::PushStyleColor(ImGuiCol_FrameBg,IM_COL32(255,85,85,80));
				ImGui::InputText("###Expression", s.mExpressionInput, sizeof(s.mExpressionInput));
				if(red)
					ImGui::PopStyleColor();
				if(*s.mExpressionInput)
				{
					ImGui::TableNextRow();
					ImGui::TableSetColumnIndex(0);
					if(ImGui::Button("Apply",  ImVec2(ImGui::GetContentRegionAvail().x,0)))
					{
						Command cmd;
						cmd.ctype = EVENT_POLL_MAP_AXIS;
						cmd.datasize = 0;
						cmd.gui = true;
						cmd._AddStr(s.mMappingToolSelectedAction, 0);
						cmd.args[1].i = s.mMappingToolActionHand;
						cmd.args[2].i = s.mMappingToolActionAxis - 1;
						cmd._AddStr(SubStrB(s.mExpressionInput), 3);
						gClient.Send(cmd, TARGET_APP, as.pid);
						if(cmd.args[1].i == 4)
						{
							cmd.args[1].i = 1;
							gClient.Send(cmd, TARGET_APP, as.pid);
							cmd.args[1].i = 0;
							gClient.Send(cmd, TARGET_APP, as.pid);
						}
					}
					ImGui::TableSetColumnIndex( 1 );
					SBPrint( command_example,  "%s %s %d %d %s", gCommands[EVENT_POLL_MAP_AXIS].name.begin, s.mMappingToolSelectedAction.begin, s.mMappingToolActionHand, s.mMappingToolActionAxis - 1, s.mExpressionInput );

					if(ImGui::SmallButton("conf"))
						ImGui::OpenPopup(s.mSessionName);
					ImGui::SameLine();
					if(ImGui::SmallButton("copy"))
						ImGui::SetClipboardText(command_example);
					ImGui::SameLine();
					ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
					ImGui::InputText("##Command example", command_example, sizeof(command_example), ImGuiInputTextFlags_ReadOnly);

					if(ImGui::BeginPopup(s.mSessionName))
					{
						ImGui::PushStyleVar(ImGuiStyleVar_ChildBorderSize, 0);
						ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));
						if (ImGui::BeginChild("ResizableChild", ImVec2(300, ImGui::GetTextLineHeightWithSpacing() * 4), ImGuiChildFlags_Border | ImGuiChildFlags_ResizeY))
						{
							ImGui::TextUnformatted("Config example");
							SBPrint( config_example, "[actionmap.example_%s_%s]\naxis%d = %s\n[bindings.example]\n%s%s = example_%s_%s\n", s.mMappingToolSelectedAction.begin, gUserDevNames[s.mMappingToolActionHand], s.mMappingToolActionAxis, s.mExpressionInput, s.mMappingToolSelectedAction.begin, gszUserMappingSuffixes[s.mMappingToolActionHand], s.mMappingToolSelectedAction.begin, gUserDevNames[s.mMappingToolActionHand]);
							ImGui::InputTextMultiline("##Config example", config_example, sizeof(config_example), ImGui::GetContentRegionAvail(), ImGuiInputTextFlags_ReadOnly);
							ImGui::EndChild();
							if(ImGui::Button("Copy to Clipboard"))
								ImGui::SetClipboardText(config_example);
						}
						ImGui::PopStyleVar(2);
						ImGui::EndPopup();
					}
				}
			}
		}
	}
	ImGui::EndTable();
}

void DrawCurrentActionMaps(AppState &as, AppSessionState &s)
{
	char maps_buf[1024], profiles_buf[1024] = "[bindings.example]\n";
	int maps_pos = 0, profiles_pos = sizeof("[bindings.example]");
	if(!ImGui::BeginTable("##cur_profile", 6, ImGuiTableFlags_BordersOuter| ImGuiTableFlags_RowBg))
		return;
	ImGui::TableSetupColumn("Action", ImGuiTableColumnFlags_WidthFixed);
	ImGui::TableSetupColumn("Typ",  ImGuiTableColumnFlags_WidthFixed);
	ImGui::TableSetupColumn("Ax",  ImGuiTableColumnFlags_WidthFixed);
	ImGui::TableSetupColumn("Idx",  ImGuiTableColumnFlags_WidthFixed);
	ImGui::TableSetupColumn("Mapping",  ImGuiTableColumnFlags_WidthStretch);
	ImGui::TableSetupColumn("Dev",  ImGuiTableColumnFlags_WidthFixed);
	ImGui::TableHeadersRow();
	ImGui::TableNextRow();
	// begin table
	for(int i = 0; i < USER_PATH_COUNT; i++)
	{
		HASHMAP_FOREACH(s.mActionMaps[i], node)
		{
			if(!node->v.override)
				continue;
			// col0
			ImGui::TableSetColumnIndex(0);
			ImGui::Text("%s %s", node->v.actName.val, gUserDevNames[i]);
			maps_pos += SNPrint( &maps_buf[maps_pos], 1024 - maps_pos, "[actionmap.example_%s_%s]\n", node->v.actName.val, gUserDevNames[i]) - 1;
			profiles_pos += SNPrint( &profiles_buf[profiles_pos], 1024 - profiles_pos, "%s%s = example_%s_%s\n", node->v.actName.val, gszUserMappingSuffixes[i], node->v.actName.val, gUserDevNames[i]) - 1;
			ImGui::TableSetColumnIndex(1);
			ImGui::Text("%d", node->v.actType.val);
			if(node->v.mapIndex >= 0)
			{
				// col1, col2, ,col3, col5
				ImGui::TableSetColumnIndex(2);
				ImGui::Text("D");
				ImGui::TableSetColumnIndex(3);
				ImGui::Text("%d", node->v.mapIndex.val);
				ImGui::TableSetColumnIndex(4);
				//ImGui::Text("Direct mapping %d %d %s", node->v.actType.val, node->v.mapIndex.val, gUserDevNames[node->v.handIndex.val]);
				if(node->v.actType > 0 && node->v.actType < 4)
				{
					AppSource *src = s.mSourceIndexes[node->v.actType - 1][node->v.mapIndex | ((unsigned int)node->v.handIndex) << 16];
					// col4, popup
					if(src)
					{
						char ptr[32];
						SBPrint(ptr, "%d%d%p", i, node->v.actType - 1, &node->v );
						ImGui::PushID(ptr);
						if(ImGui::SmallButton("src"))
							ImGui::OpenPopup(ptr);
						ImGui::SameLine();
						ImGui::Text("%s", src->name.val);
						if(ImGui::BeginPopup(ptr))
						{
							ImGui::SetNextItemOpen(true, ImGuiCond_Once);
							ImGuiTreeDumper d{"Source"};
							DumpNamedStruct(d, src);
							ImGui::EndPopup();
						}
						ImGui::PopID();
						maps_pos += SNPrint( &maps_buf[maps_pos], 1024 - maps_pos, "map = %s%s\n", src->name.val, gszUserMappingSuffixes[node->v.handIndex.val]) - 1;
					}
					else if(ImGui::SmallButton("get sources"))
					{
						Command cmd;
						cmd.ctype = EVENT_POLL_DUMP_SOURCES;
						cmd.datasize = 0;
						cmd.gui = true;
						gClient.Send(cmd, as);
					}
				}
				ImGui::TableSetColumnIndex(5);
				ImGui::Text("%s", gUserDevNames[node->v.handIndex.val]);
				ImGui::TableNextRow();
			}
			if(node->v.hasAxisMapping)
			{
				if(node->v.actionIndex1 >= 0 && node->v.funcIndex1 > 0 && node->v.funcIndex1 <= 5)
				{
					ImGui::TableSetColumnIndex(2);
					ImGui::Text("1");
					ImGui::TableSetColumnIndex(3);
					ImGui::Text("%d", node->v.actionIndex1.val);
					ImGui::TableSetColumnIndex(4);
					if(node->v.funcIndex1 == 5)
					{
						// col1, col2
						//ImGui::Text("expression #%d", node->v.actionIndex1.val);
						AppRPN *src = s.mExpressionIndexes[node->v.actionIndex1.val];
						if(src)
						{
							char ptr[32];
							SBPrint(ptr, "%d%d%pe1", i, node->v.actionIndex1.val, &node->v );
							ImGui::PushID(ptr);
							if(ImGui::SmallButton("exp"))
								ImGui::OpenPopup(ptr);
							ImGui::SameLine();
							ImGui::Text("%s", src->source.val);
							if(ImGui::BeginPopup(ptr))
							{
								ImGui::SetNextItemOpen(true, ImGuiCond_Once);
								ImGuiTreeDumper d{"Expression"};
								DumpNamedStruct(d, src);
								ImGui::EndPopup();
							}
							ImGui::PopID();
							maps_pos += SNPrint( &maps_buf[maps_pos], 1024 - maps_pos, "axis1 = %s\n", src->source.val) - 1;
						}
						else if(ImGui::SmallButton("get expressions"))
						{
							Command cmd;
							cmd.ctype = EVENT_POLL_DUMP_EXPRESSIONS;
							cmd.datasize = 0;
							cmd.gui = true;
							gClient.Send(cmd, as);
						}
					}
					else
					{
						// col1, col2, col4
						//ImGui::Text("axis1 mapped to source type%d #%d %s axis%d", node->v.actType.val, node->v.actionIndex1.val, gUserDevNames[node->v.handIndex1.val], node->v.axisIndex1.val);
						AppSource *src = s.mSourceIndexes[node->v.funcIndex1 - 1][node->v.actionIndex1.val | ((unsigned int)node->v.handIndex1.val) << 16];
						if(src)
						{
							char ptr[32];
							SBPrint(ptr, "%d%d%p1", i, node->v.funcIndex1 - 1, &node->v );
							ImGui::PushID(ptr);
							if(ImGui::SmallButton("src"))
								ImGui::OpenPopup(ptr);
							ImGui::SameLine();
							ImGui::Text("%s", src->name.val);
							if(ImGui::BeginPopup(ptr))
							{
								ImGui::SetNextItemOpen(true, ImGuiCond_Once);
								ImGuiTreeDumper d{"Source"};
								DumpNamedStruct(d, src);
								ImGui::EndPopup();
							}
							ImGui::PopID();
							maps_pos += SNPrint( &maps_buf[maps_pos], 1024 - maps_pos, "axis1 = %s%s\n", src->name.val, gszUserMappingSuffixes[node->v.handIndex1.val]) - 1;
						}
						else if(ImGui::SmallButton("get sources"))
						{
							Command cmd;
							cmd.ctype = EVENT_POLL_DUMP_SOURCES;
							cmd.datasize = 0;
							cmd.gui = true;
							gClient.Send(cmd, as);
						}
						ImGui::TableSetColumnIndex(5);
						ImGui::Text("%s[%d]",gUserDevNames[node->v.handIndex1.val], node->v.axisIndex1.val);
					}
					maps_pos += SNPrint( &maps_buf[maps_pos], 1024 - maps_pos, "\n") - 1;
					ImGui::TableNextRow();
				}
				if(node->v.actionIndex2 >= 0 && node->v.funcIndex2 > 0 && node->v.funcIndex2 <= 5)
				{
					ImGui::TableSetColumnIndex(2);
					ImGui::Text("2");
					ImGui::TableSetColumnIndex(3);
					ImGui::Text("%d", node->v.actionIndex2.val);
					ImGui::TableSetColumnIndex(4);
					if(node->v.funcIndex2 == 5)
					{
						// col1, col2
						//ImGui::Text("expression #%d", node->v.actionIndex1.val);
						AppRPN *src = s.mExpressionIndexes[node->v.actionIndex2.val];
						if(src)
						{
							char ptr[32];
							SBPrint(ptr, "%d%d%pe2", i, node->v.actionIndex2.val, &node->v );
							ImGui::PushID(ptr);
							if(ImGui::SmallButton("exp"))
								ImGui::OpenPopup(ptr);
							ImGui::SameLine();
							ImGui::Text("%s", src->source.val);
							if(ImGui::BeginPopup(ptr))
							{
								ImGui::SetNextItemOpen(true, ImGuiCond_Once);
								ImGuiTreeDumper d{"Expression"};
								DumpNamedStruct(d, src);
								ImGui::EndPopup();
							}
							ImGui::PopID();
							maps_pos += SNPrint( &maps_buf[maps_pos], 1024 - maps_pos, "axis2 = %s\n", src->source.val) - 1;
						}
						else if(ImGui::SmallButton("get expressions"))
						{
							Command cmd;
							cmd.ctype = EVENT_POLL_DUMP_EXPRESSIONS;
							cmd.datasize = 0;
							cmd.gui = true;
							gClient.Send(cmd, as);
						}
					}
					else
					{
						// col1, col2, col4
						//ImGui::Text("axis1 mapped to source type%d #%d %s axis%d", node->v.actType.val, node->v.actionIndex1.val, gUserDevNames[node->v.handIndex1.val], node->v.axisIndex1.val);
						AppSource *src = s.mSourceIndexes[node->v.funcIndex2 - 1][node->v.actionIndex2.val | ((unsigned int)node->v.handIndex2.val) << 16];
						if(src)
						{
							char ptr[32];
							SBPrint(ptr, "%d%d%p2", i, node->v.funcIndex2 - 1, &node->v );
							ImGui::PushID(ptr);
							if(ImGui::SmallButton("src"))
								ImGui::OpenPopup(ptr);
							ImGui::SameLine();
							ImGui::Text("%s", src->name.val);
							if(ImGui::BeginPopup(ptr))
							{
								ImGui::SetNextItemOpen(true, ImGuiCond_Once);
								ImGuiTreeDumper d{"Source"};
								DumpNamedStruct(d, src);
								ImGui::EndPopup();
							}
							ImGui::PopID();
							maps_pos += SNPrint( &maps_buf[maps_pos], 1024 - maps_pos, "axis2 = %s%s\n", src->name.val, gszUserMappingSuffixes[node->v.handIndex2.val]) - 1;
						}
						else if(ImGui::SmallButton("get sources"))
						{
							Command cmd;
							cmd.ctype = EVENT_POLL_DUMP_SOURCES;
							cmd.datasize = 0;
							cmd.gui = true;
							gClient.Send(cmd, as);
						}
						ImGui::TableSetColumnIndex(5);
						ImGui::Text("%s[%d]",gUserDevNames[node->v.handIndex2.val], node->v.axisIndex2.val);
					}
					maps_pos += SNPrint( &maps_buf[maps_pos], 1024 - maps_pos, "\n") - 1;
					ImGui::TableNextRow();
				}
			}
		}
	}
	ImGui::EndTable();
	if(maps_pos)
	{
		maps_pos += SNPrint( &maps_buf[maps_pos], 1024 - maps_pos, "\n") - 1;
		if(profiles_pos + maps_pos > 1023)
			profiles_pos -= profiles_pos + maps_pos - 1023;
		memcpy(&maps_buf[maps_pos], profiles_buf, profiles_pos);
		maps_pos += profiles_pos;
		maps_buf[maps_pos] = 0;
		ImGui::TextUnformatted("Config example:");
		ImGui::PushStyleVar(ImGuiStyleVar_ChildBorderSize, 0);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));
		if (ImGui::BeginChild("ResizableChild", ImVec2(-FLT_MIN, ImGui::GetTextLineHeightWithSpacing() * 4), ImGuiChildFlags_Border | ImGuiChildFlags_ResizeY))
		{
			ImGui::InputTextMultiline("###Config example", maps_buf, maps_pos, ImGui::GetContentRegionAvail(), ImGuiInputTextFlags_ReadOnly );
			ImGui::EndChild();
		}
		ImGui::PopStyleVar(2);
	}
	if(ImGui::Button("Update data"))
	{
		Command cmd;
		cmd.ctype = EVENT_POLL_DUMP_SOURCES;
		cmd.datasize = 0;
		cmd.gui = true;
		gClient.Send(cmd, as);
		cmd.ctype = EVENT_POLL_DUMP_EXPRESSIONS;
		gClient.Send(cmd, as);
		cmd.ctype = EVENT_POLL_DUMP_ACTION_MAPS;
		gClient.Send(cmd, as);
	}
	if(maps_pos)
	{
		ImGui::SameLine();
		if(ImGui::Button("Copy to Clipboard"))
			ImGui::SetClipboardText(maps_buf);
	}

}

void DrawMappingToolTab(AppState &as, AppSessionState &s)
{
	ImGui::SetNextItemOpen(true, ImGuiCond_Once);
	if(ImGui::CollapsingHeader("Add/Replace mapping"))
		DrawCreateMapping(as,s);
	ImGui::SetNextItemOpen(true, ImGuiCond_Once);
	if(ImGui::CollapsingHeader("Current profile"))
		DrawCurrentActionMaps(as,s);
}

void DrawSessionWindow(AppState &as, AppSessionState &s)
{
	ImGui::SetNextWindowSize(ImVec2(555,450), ImGuiCond_Once);
	ImGui::Begin( s.mSessionName, &s.show );
	if(s.mState.state == SESSOION_DESTROY)
		ImGui::TextColored(ImVec4(1,0,0,1), "Warning: session destroyed, data will not update!");
	if(as.mState != AppRunning)
		ImGui::TextColored(ImVec4(1,0,0,1), "Warning: application is not running!");
	if(ImGui::BeginTabBar("##tabs"))
	{
		if(ImGui::BeginTabItem("Mapping tool"))
		{
			DrawMappingToolTab(as, s);
			ImGui::EndTabItem();
		}
		if(ImGui::BeginTabItem("Action sets"))
		{
			DrawActionSetTab(as, s);
			ImGui::EndTabItem();
		}
		if(ImGui::BeginTabItem("Sources"))
		{
			DrawSourcesTab(as, s);
			ImGui::EndTabItem();
		}
		if(ImGui::BeginTabItem("Action maps"))
		{
			DrawActionMapsTab(as, s);
			ImGui::EndTabItem();
		}
		if(ImGui::BeginTabItem("Expressions"))
		{
			DrawExpressionsTab(as, s);
			ImGui::EndTabItem();
		}
		if(ImGui::BeginTabItem("Custom actions"))
		{
			DrawCustomActionsTab(as, s);
			ImGui::EndTabItem();
		}

		ImGui::EndTabBar();
	}
	ImGui::End();
}

void DrawVariablesWindow(AppState &s)
{
	ImGui::SetNextWindowSize(ImVec2(400,500), ImGuiCond_Once);
	ImGui::Begin( s.mVarName, &s.showVars );
	if(ImGui::Button("Update"))
	{
		Command cmd;
		cmd.ctype = EVENT_POLL_DUMP_VARIABLES;
		cmd.datasize = 0;
		cmd.gui = true;
		gClient.Send(cmd, s);
	}
	ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(2, 2));
	if (ImGui::BeginTable("##split", 2, ImGuiTableFlags_Resizable | ImGuiTableFlags_BordersOuter| ImGuiTableFlags_RowBg | ImGuiTableFlags_ScrollY))
	{
		ImGui::TableSetupScrollFreeze(0, 1);
		ImGui::TableSetupColumn("Object");
		ImGui::TableSetupColumn("Contents");
		ImGui::TableHeadersRow();

		HASHMAP_FOREACH(s.mVariables, src)
		{
			ImGuiTableTreeDumper d{src->k.begin};
			d.startOpen = true;
			DumpNamedStruct(d, &src->v);
		}

		ImGui::EndTable();
	}
	ImGui::PopStyleVar();
	ImGui::End();
}

void DrawPlotsWindow(AppState &s)
{
	ImGui::SetNextWindowSize(ImVec2(400,500));
	ImGui::Begin( s.mPlots.mPlotName, &s.mPlots.show );
	ImGui::Checkbox( "Show variables", &s.mPlots.showVars );
	ImGui::Checkbox( "Show sources", &s.mPlots.showSources );
	ImGui::Checkbox( "Show Action maps", &s.mPlots.showActMaps );
	ImGui::Checkbox( "Enable update", &s.mPlots.autoUpdate );
	bool changed = ImGui::SliderFloat( "Update interval",&s.mPlots.curInterval, 0.0f, 1.0f );
	unsigned int mask = !!s.mPlots.showSources | !!s.mPlots.showActMaps << 1 | !!s.mPlots.showSources << 2;
	if(!s.mPlots.autoUpdate)
		mask = 0;
	if(mask != s.mPlots.cur_mask || changed )
	{
		Command c;
		c.ctype = EVENT_POLL_SET_WATCH;
		c.args[0].i = mask;
		c.args[1].i = ((double)s.mPlots.curInterval) * 1e9;
		c.datasize = 0;
		c.gui = true;
		gClient.Send(c,s);
		s.mPlots.cur_mask = mask;
	}

	HASHMAP_FOREACH( s.mPlots.mGraphs, graph )
	{
		if(graph->k.StartsWith("src") && !s.mPlots.showSources)
			continue;
		if(graph->k.StartsWith("map") && !s.mPlots.showActMaps)
			continue;
		if(graph->k.StartsWith("var") && !s.mPlots.showVars)
			continue;
		ImGui::PlotLines(graph->k.begin, graph->v.values.storage, IM_ARRAYSIZE(graph->v.values.storage), graph->v.counter & graph->v.values.mask, NULL, FLT_MIN, FLT_MAX, ImVec2(0, 30.0f));
	}
	ImGui::End();
}

int main(int argc, char **argv)
{
	int port = 0;
	if(argc >= 2)
	{
		port = atoi(argv[1]);
		gClient.Connect(port);
	}
	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;      // Enable Gamepad Controls

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();
	if(!Platform_Init("Layer GUI", 1024, 768, false))
		return 1;
	bool done = false;
	bool show_demo_window = false;
	bool frameSkipped = false;
	SubStr("Console").CopyTo(gConsole.mWindowName);

	while(!done)
	{
		bool hasEvents;
		done = !Platform_ProcessEvents(hasEvents);
		if(hasEvents)
			gRequestFrames = 3;
		if(gRequestFrames < 2)
		{
			FrameControl();
			if(gRequestFrames)
				gRequestFrames--;
			frameSkipped = true;
			continue;
		}
		Platform_NewFrame();
		ImGui::NewFrame();
		if (show_demo_window)
			ImGui::ShowDemoWindow(&show_demo_window);
		ImGui::SetNextWindowSize(ImVec2(400,400), ImGuiCond_FirstUseEver);
		ImGui::Begin("Client");
		ImGui::SetNextItemOpen(true, ImGuiCond_Once);
		if(ImGui::CollapsingHeader("Connection"))
		{
			ImGui::InputInt("Port", &port);ImGui::SameLine();
			if(ImGui::Button("Connect"))
			{
				gClient.Connect(port);
			}
		}

		ImGui::SetNextItemOpen(true, ImGuiCond_Once);
		if(ImGui::CollapsingHeader("Apps"))
		{
			HASHMAP_FOREACH(gApps, node)
			{
				if(node->v.mState == AppRunning && node->v.mLastSend > node->v.mLastReceive + 1000000000 && GetTimeU64() - node->v.mLastSend  >  1000000000)
					node->v.SetState(node->v.mReg.name.val, node->k, AppStale);
				else if(node->v.mState == AppStale && !(node->v.mLastSend > node->v.mLastReceive + 1000000000))
					node->v.SetState(node->v.mReg.name.val, node->k, AppRunning);
				ImGui::SetNextItemOpen(true, ImGuiCond_Once);
				if(ImGui::TreeNode(node->v.mAppName))
				{
					ImGui::SetNextItemOpen(true, ImGuiCond_Once);
					if(ImGui::TreeNode("Windows"))
					{
						ImGui::Selectable("Console", &node->v.mConsole.show);
						if(!node->v.pActiveSession)
						{
							if(ImGui::Selectable("Get active session"))
							{
								Command cmd;
								cmd.ctype = EVENT_POLL_DUMP_SESSION;
								cmd.datasize = 0;
								cmd.gui = true;
								gClient.Send(cmd, node->v);
							}
						}
						else
							ImGui::Selectable("Session", &node->v.pActiveSession->show);
						ImGui::Selectable("Variables", &node->v.showVars);
						ImGui::Selectable("Plots", &node->v.mPlots.show );
						ImGui::TreePop();
					}
					ImGui::SetNextItemOpen(true, ImGuiCond_Once);
					if(ImGui::TreeNode("Actions"))
					{
						if(ImGui::Button("Reload config"))
						{
							Command cmd;
							cmd.ctype = EVENT_POLL_RELOAD_CONFIG;
							cmd.datasize = 0;
							gClient.Send(cmd, node->v);
						}
						ImGui::SetItemTooltip("Reload configuration, clear all profile changes");
						if(ImGui::Button("Trigger reload bindings"))
						{
							Command cmd;
							cmd.ctype = EVENT_POLL_TRIGGER_INTERACTION_PROFILE_CHANGED;
							cmd.datasize = 0;
							gClient.Send(cmd, node->v);
						}
						ImGui::SetItemTooltip("This triggers INTERACTION_PROFILE_CHANGED event. App may recreate session for new bindings");
						if(ImGui::Button("Clear session info"))
						{
							node->v.mSessions.Clear();
							node->v.pActiveSession = nullptr;
						}
						ImGui::SetItemTooltip("Clear all data got from app.\nThis might be useful after profile change");
						ImGui::TreePop();
					}
					ImGuiTreeDumper d{"Registration info"};
					DumpNamedStruct(d, &node->v.mReg);
					HASHMAP_FOREACH(node->v.mSessions, ses)
					{

						ImGui::SetNextItemOpen(true, ImGuiCond_Once);
						if(ImGui::TreeNode(ses->v.mSessionName))
						{
							d.tname = "Session state";
							DumpNamedStruct(d, &ses->v.mState);
							ImGui::Checkbox("Show session window", &ses->v.show);
							ImGui::TreePop();
						}
					}
					ImGui::TreePop();
				}
			}
			ImGui::SetNextItemOpen(true, ImGuiCond_Once);
			if(ImGui::CollapsingHeader("Debug"))
			{
				ImGui::Checkbox("Show Dear ImGui Demo", &show_demo_window);
				ImGui::Checkbox("Global console", &gConsole.show);
			}

		}
		ImGui::End();
		if(gConsole.show)
			gConsole.Draw();
		HASHMAP_FOREACH(gApps, node)
		{
			if(node->v.mConsole.show)
				node->v.mConsole.Draw();
			HASHMAP_FOREACH(node->v.mSessions, ses)
				if(ses->v.show)
					DrawSessionWindow(node->v, ses->v);
			if(node->v.showVars)
				DrawVariablesWindow(node->v);
			if(node->v.mPlots.show)
				DrawPlotsWindow(node->v);
			if((!node->v.mPlots.show || done) && node->v.mPlots.cur_mask)
			{
				Command c;
				c.ctype = EVENT_POLL_SET_WATCH;
				c.args[0].i = 0;
				c.args[1].i = 0;
				c.gui = true;
				c.datasize = 0;
				gClient.Send(c,node->v);
				node->v.mPlots.cur_mask = 0;
			}
		}

		ImGui::Render();
		if(!frameSkipped)
		{
			Platform_Present(io);
			FrameControl();
			gRequestFrames--;
		}

		frameSkipped = false;
	}
	Platform_Shutdown();
}
