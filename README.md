# OpenXR binding layer

<!--
SPDX-License-Identifier: MIT
SPDX-FileCopyrightText: 2024 mittorn <mittorn@disroot.org>
-->

Advanced OpenXR binding layer

## Building

Simply run CMake, or use CMake GUI. I recommend creating a `build` subfolder.

```bash
mkdir build
cd build
cmake ..
make
```

ImGui-based configurator is included, SDL2 required. To disable GUI building, specify `-DENABLE_GUI=OFF` in CMake command

## Usage

Once the layer is built, it will be located in the `<build>/XR_APILAYER_NOVENDOR_xr_binder` directory, alongside its Json Manifest.

Many different configurations may result in an OpenXR application loading and using an API layer. Please refer to the OpenXR *Loader* specification (not the core specification) for more information.
They are platform-dependant. On Windows, they involve setting registry keys. On Linux, they involve symlinking the .json manifest files to specific places in the filesystem.

A simple way to get a Loader implicitly enabled by an openxr application is to specify a number of environment variables when starting the client program.

```
XR_API_LAYER_PATH=path_to_the_layer_output_folder/XR_APILAYER_NOVENDOR_xr_binder
XR_ENABLE_API_LAYERS=XR_APILAYER_NOVENDOR_xr_binder
```

You can also specify `XR_LOADER_DEBUG=all` to cause the loader to output all debug messages it can to standard output. This is useful for debugging layer loading issues.

For more installation about OpenXR API Layer intallation and usage, please reffer to the OpenXR Loader specification chapters about API Layers. The [discovery](https://github.com/KhronosGroup/OpenXR-SDK-Source/blob/main/specification/loader/api_layer.adoc#api-layer-discovery) section might be of particular interest to layer developers.

## Configuration

Configuration is stored in `$XDG_CONFIG_HOME/xrBinder` or in `$HOME/.config/xrBinder` folder

Copy example configuration form [example_configs](example_configs) dir

Config options read from app_<appname>.ini with fallback to xrBinder ini. This allows to configure sources and common action templates globally and write local profiles for apps

### Sources

`[source.*]` sections declare all sources which might be used by layer. Usually this configuration is common for most games, but may be depended on interaction profiles, which game is using

`actionType` is `action_bool`, `action_float` or `action_vector2`
`bindings` is comma-separated  OpenXR bindigns path, with optional interactionProfile filter, You can merge bindings from diferent source templates to represent your controller with different interaction profiles
`subactionOverride` is optional override to use different controller path/ids. That is not tested

Generated source configurations for monado-supported controllers can be found in [source_templates](example_configs/source_templates) folder

Layer only can use sources for application's current interaction profile, so source configs may differ between applications. All sources from different interaction profiles are ignored.

After defining sources, check if it's correctly accepted by runtime (dumpLayerBindings command in cli/gui clients or expand bindings layer_action_set in ActionSet session tab)

Some errors may present in app log if sources defined incorreclty

### Action maps

`[actionmap.*]` sections declare mappings from source sections to application actions. It may be mapped in two ways:

* Direct mapping: map action directly to openxr source. All types, timings are keeped unchanged
* Axis mapping: any individual axis may be mapped to any source axis or even calculated expression on it

Direct and axis mappings may be combined, overriding some data from direct mapping by axis
`map` is direct mapping specification. Type of source section must much type of application mapping
`axis1`, `axis2` is axis mapping specification. For non-vector2 actions only `axis1` is useful

### Custom actions

`[custom.*]` sections allows to do some actions or conditionaly change variables
`command` is command, which should be triggered
`condition`: before triggering command, or assign variables check this condition expression
`period` is float second interval for checking condition and doing action

### Binding profiles

`[bindings.*]` sections connects actionmap sections to application actions and which custom actions are enabled. Many actions may have same actionmap

### Expressions

Axis mappings and custom actions use simple expression syntax

It is not complete scripting language, but it allows implement many useful calculations

Supported math operations: `*/%+-=><`, operator not: `!`, brackets `()`

Supported functions:
* math constants:
```
CONST_PI()
CONST_E()
```
* math with single parameter:
```
cos
exp
tan
atan
sinh
cosh
tanh
log
log10
sqrt
fabs
acosh
asinh
atanh
abs
```
* math two parameters:
```
fmod
pow
atan2
max
min
```
* glsl-like:

```
smoothstep
mix
step
fract
clamp
sign
```
* other:
```
randuniform() - random with uniform distrubution
randnormal() - random with normal distribution
frameStartTime() - frame start time in seconds
frameTime() - frametime in seconds
frameCount() - frame counter
displayTime() - predicted frame display time from runtime
displayDeltaTime() - delta from predicted times
displayPeriod() - predicted framerate, usually is interval of HMD display frequency
shouldRender() - 1 if runtime is requesting frames
fps() - real rendered fps
fpsPredicted() - runtime predicted fps
delay(x) - store and return previous x value
delay2(x) - like delay, but returns value 2 samples ago
delta(x) - difference from prevous value. Useful for derivatives
median(x) - use 3-samples median filter on value. Filtered value is delayed to one sample https://en.wikipedia.org/wiki/Median_filter
movavg2(x), movavg3(x) - moving average filters https://en.wikipedia.org/wiki/Moving_average
changed(x) - 1 of x changed form previous sample
front(x) - 1 if previous value is 0 and current is 1
back(x) 1 if current value is 0, but previous is 1
pressDuration(x) - duration of x being is non-zero continously
checkLongClick(x, duration) - 1 after x is long-clicked with specified duration
checkDoubleClick(x, interval) - 1 after x is double-clicked with specified interval
bounds(min,x,max) - bound x value with min/max
middle(x,y,z) - median function used in median filter
sel(cond,a,b) - ternary operator (but both expressions are calculated)
```

Tokens starting with `$` are handled as variables

Inline variable assignment supported, `($var1 = $var2 * 2) will change var1`, but always running unconditionally

To do conditional assignment, always assign old value in false condition: (`$var1 = $var2 * (condition) + $var1 * !(condition)` or `$var1 = sel(condition, $var2, $var1)`) or use custom action with condition

### IPC

To run graphical configurator, setup IPC

There are 2 modes: server and bus

* In server mode application will run as udp server on `serverPort` (root config section). This might break if application create multiple instances or starting multiple applications

* In bus mode, all applications connects to ipc_server on `serverPort`. This is recommended way

You should start `ipc_server <port>` before running application (You may use start-stop-daemon to run it in background)

After ipc set up, run `client_gui <port>` or `client_simple <port>` to debug and edit configuration

## Roadmap

* CI builds
* Poses input, but not binding. Mapping poses will need wrap everything that relies on it's spaces and it is likely to break any possible extension
* Config includes for defining multiple interaction profiles depending on app
* Match additional envs for config name, like Steam AppID for proton or proper executable name for wine
* Proper usage of DiagMsg for config and source errors, show modal message boxes in GUI
* Hiding interaction profiles from application, forcing it to use different profile
* OSC commands, OSC variable substitution
* Optional shell commands (disabled by default of course)
* Better session management. Move attached action sets to different list
* SDL3, GLFW support in imgui platform code
* ImGui docking plots when ImGui 2.x released
* ImStrv migration when merged for master
* Vibration output command

### Unlikely, but possible

* Overlay mode, visual indication
* Windows support

## Legal

Copyright (C) 2024 mittorn, distributed under the MIT license agreement. (REUSE 3.0 compliant.) 

Based on [OpenXR-API-Layer-Template](https://github.com/Ybalrid/OpenXR-API-Layer-Template)

Modified template without STL availiable: [new_template branch](https://github.com/mittorn/OpenXR-API-Layer-Template/tree/new_template)

This is built on top of the OpenXR SDK. The OpenXR SDK is distributed by the Khronos group under the terms of the Apache 2.0 License. 
This template project does not link against any binaries from the OpenXR SDK but includes header files generated from the OpenXR registry and the standard OpenXR loader project.

*OpenXR™ is a trademark owned by The Khronos Group Inc. and is registered as a trademark in China, the European Union, Japan, and the United Kingdom.*
