import sys
import json
import os
#action_types = {'button':'action_bool','trigger':'action_float','trackpad':'action_vector2','joystick':'action_vector2'}
allowed_types = {'button', 'trigger', 'trackpad', 'joystick'}
action_types = {'click': 'bool', 'touch' : 'bool', 'value' : 'float', 'force' : 'float', 'position': 'vector2', 'ready_ext': 'bool', 'x': 'float', 'y' : 'float'}
# path to monado's bindings.json
f = open(sys.argv[1])
#d = os.mkdir(sys.argv[2])
bindings = json.load(f)
f.close()
# if true, writes full interaction profile to bindings line, allowng config work on different interaction profiles
full_paths = True
# Duplicate bool sources as float, allowing direct map floats to bools in runtime without creating axis mapping
bool_floats = True
for i in bindings['profiles']:
	profile_name = i.split('/')[-1]
	f = open(sys.argv[2] + '/' + profile_name + '.ini','wb')
	for j in bindings['profiles'][i]['subpaths']:
		s = bindings['profiles'][i]['subpaths'][j]
		if not s['type'] in allowed_types:
			continue
		c = s['components']
		if 'position' in c:
			c.append('x')
			c.append('y')

		for k in c:
			f.write('[source.' + j.split('/')[-1] + '_' + k + ']\n')
			f.write('actionType = action_' + action_types[k]+'\n')
			bnd = []
			for l in bindings['profiles'][i]['subaction_paths']:
				override  = ''
				if 'side' in s:
					if not s['side'] in l:
						continue
					override = l
				elif not 'right' in l and not 'left' in l:
					override = l
				path = l + j
				if k != 'position':
					path = path + '/' + k
				if full_paths:
					path = i + ':' + path
				bnd.append(path)
			if override:
				f.write('subactionOverride = ' + override + '\n')
			f.write('bindings = '+','.join(bnd)+'\n')
			if bool_floats and action_types[k] == 'bool':
				f.write('[source.' + j.split('/')[-1] + '_' + k + '_float]\n')
				f.write('actionType = action_float\n')
				bnd = []
				for l in bindings['profiles'][i]['subaction_paths']:
					override  = ''
					if 'side' in s:
						if not s['side'] in l:
							continue
						override = l
					elif not 'right' in l and not 'left' in l:
						override = l
					path = l + j
					if k != 'position':
						path = path + '/' + k
					if full_paths:
						path = i + ':' + path
					bnd.append(path)
				if override:
					f.write('subactionOverride = ' + override + '\n')
				f.write('bindings = '+','.join(bnd)+'\n')
				